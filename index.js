const fs    = require('fs')
const path  = require('path')

let Parser = require('./classes/TSVDataParser.js').TSVDataParser

let inDir   = './input',
    outDir  = './output',
    inFiles = fs.readdirSync(inDir).filter(f=>f.match(/\.(txt|tsv)/)!=null),
    outJson = path.join(outDir, 'json'),
    outXml  = path.join(outDir, 'xml'),
    dirs    = [outDir, outJson, outXml]

dirs.map(dir=>{
    // ensure outdirs exists
    if (!fs.existsSync(dir))
        fs.mkdirSync(dir)
})

inFiles.map(filename=>{
    let tag = filename.replace(/s?\.txt/, '') // outer tag per xml row item
    new Parser({
        inDir: inDir, 
        inFile: filename, 
        outDir: outDir, 
        makeTrunc: true, // makes a truncated version, removes empty val columns
        rowTag: tag
    })
})
