const fs        = require('fs')
const path      = require('path')
const json2xml  = require('jsontoxml')

let tsv2json = filecontents => {
    let lines = filecontents.split('\n'),
        headers = lines.slice(0, 1)[0].split('\t')

    return lines.slice(1, lines.length).map(line => {
        let data = line.split('\t')
        return headers.reduce((obj, nextKey, index) => {
            obj[nextKey] = (data[index]||'').trim()
            return obj;
        }, {})
    })
}

class TSVDataParser {
    constructor(cfg) {
        let { // deconstruct config members to this scope with default args
            inDir       = undefined,
            inFile      = undefined,
            outDir      = undefined,
            makeTrunc   = false,// creates truncated version ommitting empties
            rowTag      = 'Row',// wrapper for rows in xml
        } = (cfg ? cfg : {})

        this.inDir      = inDir
        this.inFile     = inFile
        this.outDir     = outDir
        this.makeTrunc  = makeTrunc
        this.rowTag     = rowTag

        this.json = {} // will put data here

        this.inFileName = path.parse(inFile).name
        this.inFilePath = path.join(inDir, inFile)

        let contents = fs.readFileSync(this.inFilePath, 'utf8')

        this.parse(contents)
    }

    parse(contents) {
        this.parseToJson(contents)
        this.parseToXml(contents)
    }

    parseToJson(contents) {
        let data = tsv2json(contents),
            ext = 'json',
            outFile = [this.inFileName, ext].join('.'),
            outFilePath = path.join(this.outDir, 'json', outFile),
            tsvcfg = {
                input: this.inFilePath,
            }

        this.writeJsonToFile([...data], outFilePath)

        // truncated output
        if (this.makeTrunc) {
            // alter name/path for truncated version
            outFile = [this.inFileName, 'trunc', ext].join('.')
            outFilePath = path.join(this.outDir, 'json', outFile)
            this.writeJsonToFile(this.truncateJson([...data]), outFilePath)
        }
    }

    truncateJson(data) {
        let result = {}
        return data.map(d=>{
            let entry = {},
                keys = Object.keys(d)

            // insert non-empty data into `entry` obj
            keys.map(key=>{
                let value   = d[key]

                // do nothing if value for key is empty
                if (!value)
                    return

                // coerce to number
                if (!isNaN(value))
                    value = parseFloat(value)

                entry[key] = value
            })

            return entry
        })
    }

    writeJsonToFile(json, path) {
        let jsonString = JSON.stringify(json, 4, 4)
        fs.writeFileSync(path, jsonString, 'utf8')
    }

    parseToXml(contents) {
        let data            = tsv2json(contents),
            ext             = 'xml',
            outFile         = [this.inFileName, ext].join('.'),
            outFilePath     = path.join(this.outDir, 'xml', outFile),
            xmlcfg = {
                prettyPrint: true,
                removeIllegalNameCharacters: true,
                html: true,
            },
            truncated = this.makeTrunc ? this.truncateJson(data) : undefined

        let json = {}
        // need to wrap in root outer tag, then outer tags for each elem
        json[this.inFileName] = []
        Object.keys(data).map(k=>{
            let wrapped = {}
            wrapped[this.rowTag] = data[k]
            json[this.inFileName].push(wrapped)
        })
        this.writeXmlToFile(json2xml(json, xmlcfg), outFilePath)

        // truncated output
        if (truncated) {
            // alter name/path for truncated version
            outFile = [this.inFileName, 'trunc', ext].join('.')
            outFilePath = path.join(this.outDir, 'xml', outFile)

            let json = {}
            // need to wrap in outer tag, then outer tags for each elem
            json[this.inFileName] = []
            Object.keys(truncated).map(k=>{
                let wrapped = {}
                wrapped[this.rowTag] = truncated[k]
                json[this.inFileName].push(wrapped)
            })
            this.writeXmlToFile(json2xml(json, xmlcfg), outFilePath)
        }

    }

    writeXmlToFile(xml, path) {
        fs.writeFileSync(path, xml, 'utf8')
    }

}

module.exports.TSVDataParser = TSVDataParser

