# Setup
clone this repo, then:
```
npm install
mkdir input
```

copy TSV files with extensions `.txt` or `.tsv` into `input` directory.

# Run
to parse the files, run:
```
node index.js
```
